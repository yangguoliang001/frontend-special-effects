
const btn = document.querySelector('button')

btn.onclick = async function () {
  const handle = await showDirectoryPicker()
  await processHandle(handle)
  console.log('handle', handle)

  //获取文件夹下的子文件
  const fileHandle = handle.children

  //循环在body里添加了根目录里所有文件夹及文件的文件名
  for (let i = 0; i < fileHandle.length; i++) {
    //创建文件夹div;
    var divElement = document.createElement('div');
    //设置div里内容为文件名
    divElement.textContent = fileHandle[i].name;
    //设置div点击事件 在控制台打印 文件内容
    divElement.onclick = async function () {
      const content = await readFileContent(fileHandle[i]);
      console.log(content);
    }
    //将div添加到body中
    document.body.appendChild(divElement);
  }

}


async function processHandle(handle) {

  if (handle.kind === 'file') {
    return
  }
  handle.children = []
  const entries = await handle.entries()
  // 遍历子文件夹
  for await (const entry of entries) {
    await processHandle(entry[1])
    handle.children.push(entry[1]) // 把子文件夹句柄放到children中
  }
}

async function readFileContent(handle) {
  return new Promise(async (resolve, reject) => {
    //判断是否为文件夹
    if (handle.kind == 'directory') {
      //如果是文件夹 则不做处理
      return
    }
    // 获取文件
    const file = await handle.getFile()

    // 创建一个FileReader对象
    const reader = new FileReader()
    // 为reader对象设置onerror回调函数，当读取失败后会调用该函数
    reader.onerror = function (e) {
      reject(e)
    }
    // 为reader对象设置onload回调函数，当读取完成后会调用该函数
    reader.onload = function (e) {
      resolve(e.target.result)
    }
    //读取文件内容
    reader.readAsText(file, 'utf-8')
  })
}
