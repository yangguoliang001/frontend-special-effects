const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// 移动坐标系
ctx.translate(canvas.width / 2, canvas.height);
// y轴反向
ctx.scale(1, -1);

drawBranch([0, 0], 180, 20, 90);

function drawBranch(v0, length, branchWidth, angle, branchColor="#333") {
  if (branchWidth < 10 && Math.random() < 0.3) {
    return
  }
  if (branchWidth < 2) {
    ctx.beginPath();
    // 画圆 开始位置 半径 开始和结束角度
    ctx.arc(...v0, 10, 0, Math.PI * 2);
    ctx.fillStyle = "#fff";
    ctx.fill()
    return
  }
  ctx.beginPath();
  ctx.moveTo(...v0);
  const v1 = [
    v0[0] + length * Math.cos(angle * Math.PI / 180),
    v0[1] + length * Math.sin(angle * Math.PI / 180)
  ]
  ctx.lineTo(...v1);
  ctx.lineWidth = branchWidth;
  ctx.fillStyle = branchColor;
  ctx.lineCap = "round";
  ctx.stroke();
  drawBranch(v1, length * 0.8, branchWidth * 0.8, angle + Math.random() * 30, branchColor)
  drawBranch(v1, length * 0.8, branchWidth * 0.8, angle + Math.random() * -30, branchColor)
}
