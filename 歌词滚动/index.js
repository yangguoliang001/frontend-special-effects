

/**
 * 解析歌词
 * @param {string} lrcStr 歌词字符串
 * @return {Array} lrcList 歌词数组 { time: 0, word: "" }
 */
function parseLrc(lrcStr) {
  return lrcStr.split(/\n/)
  .filter(i => i)
  .map(i => {
    const timeReg = /\[(\d{2}):(\d{2})\.(\d{2,3})\]/
    const time = i.match(timeReg);
    if (time) {
      return {
        time: +time[1] * 60 + +time[2] + +time[3] / 1000,
        word: i.replace(timeReg, ""),
      };
    }
  });
}

/**
 * 查找时间对应的歌词下标
 * @param {*} lrcList 歌词数组
 * @param {*} time 播放时间
 * @returns
 */
function findIndex(lrcList, time) {
  const nextIndex = lrcList.findIndex(i => i.time > time);
  if (nextIndex >  0) {
    return nextIndex - 1
  }
  return lrcList.length - 1
}
/**
 * 创建歌词元素
 * @param {*} lrcList
 */
function createLrcElement(lrcList) {
  for (let i = 0; i < lrcList.length; i++) {
    const lrc = lrcList[i];
    const li = document.createElement("li");
    li.textContent = lrc.word
    doms.lrcListEl.appendChild(li)
  }
}

/**
 * 设置ul的偏移
 * @param {*} params
 */
function setOffset(params) {
  const {
    lrcList,
    currentTime,
    lyricHeight,
    liHeight,
  } = params
  const index = findIndex(lrcList, currentTime)
  if (index > 0) {
    let offset = index * liHeight + liHeight / 2 - lyricHeight / 2
    offset < 0 && (offset = 0)
    // 移除之前的active
    const li = doms.lrcListEl.querySelector(".active")
    li && li.classList.remove("active")
    doms.lrcListEl.style.transform = `translateY(${-offset}px)`
    doms.lrcListEl.children[index].classList.add("active")
  }
}

const doms = {
  audioEl: document.querySelector("audio"),
  lyricEl: document.querySelector(".lyric"),
  lrcListEl: document.querySelector(".lrc-list"),
}

const lrcList = parseLrc(lrc)
createLrcElement(lrcList)
 console.log('====================================');
 console.log(lrcList);
 console.log('====================================');
const lyricHeight = doms.lyricEl.clientHeight;
const liHeight = doms.lrcListEl.children[0].clientHeight;

doms.audioEl.addEventListener("timeupdate", () => {
  // 当前播放器的时间
  const currentTime = doms.audioEl.currentTime;
  setOffset({
    lrcList,
    currentTime,
    lyricHeight,
    liHeight,
  })
})
