const innerEl = document.querySelector('.inner')
const leftCircle = document.querySelector('.left-circle')
const rightCircle = document.querySelector('.right-circle')

let timer = null
let loader = 0
let total = 1000
timer = setInterval(() => {
  // 展示的数据大小
  let num = Number((loader / total) * 100).toFixed(1)
  // 旋转角度
  let deg = Number((loader / total) * 360).toFixed(0)
  if (num > 100) {
    clearInterval(timer)
  }else {
    loader++
    innerEl.textContent = num + '%'
    if (deg > 180) {
      leftCircle.style.transform = `rotate(${deg}deg)`
    }else {
      rightCircle.style.transform = `rotate(-${180-deg}deg)`

    }
  }
}, 2);
