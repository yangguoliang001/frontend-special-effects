class TrafficLight {
  constructor(lights) {
    this._lights = lights;
    this._currentIndex = 0; // 当前灯的索引
    this._switchTime = Date.now(); // 上次切换灯的时间
  }

  get _current() {
    return this._lights[this._currentIndex]
  }

  get _disTime() {
    return Date.now() - this._switchTime
  }

  _update() {
    // // 获取当前灯的剩余时间
    // const {lasts} = this._current
    // // 如果剩余时间小于0，则切换灯
    // if (this._disTime > lasts) {
    //   this._switchTime = Date.now()
    //   this._currentIndex = (this._currentIndex + 1) % this._lights.length
    // }

    while (1) {
      if (this._disTime < this._current.lasts) {
        break
      }

      this._switchTime += this._current.lasts
      this._currentIndex = (this._currentIndex + 1) % this._lights.length
    }
  }


  getCurrentLight() {
    // 更新灯的状态
    this._update()
    console.log('this._curren', this._current)
    return {
      color: this._current.color,
      remain: this._current.lasts - this._disTime,
    }
  }
}

export {
  TrafficLight
}
